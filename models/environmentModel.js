const include = require('../routes/includeRouter/include');

const mongoose = include.mongoose;
const moment = include.moment;

const EnvironmentSchema = new mongoose.Schema({
    building_name: { type: String },
    environment_type: { type: String },
    updated_at: { type: String, default: moment().format() },
    status_repair: { type: Boolean, default: false },
    relay_by_software: [{ type: Boolean }],
    relay_by_hardware: [{ type: Boolean }],
    detail_data: [{ // untuk data kirim
        _id: false,
        temperature: [{ type: Number }],
        humidity: [{ type: Number }],
        voltage: [{ type: Number }],
        smoke: [{ type: Number }],
        intensity: [{ type: Number }],
        gas_total: [{ type: Number }],
        updated_at: { type: String, default: moment().format() },
    }],
    detail_parameter: [{ // untuk data terima
        _id: false,
        temp_thresh: [{ type: Number }],
        hum_thresh: [{ type: Number }],
        volt_thresh: [{ type: Number }],
        smoke_thresh: [{ type: Number }],
        updated_at: { type: String, default: moment().format() },
    }],

    detail_alert: [{
        _id: false,
        temperature: [{ type: Number }],
        humidity: [{ type: Number }],
        voltage: [{ type: Number }],
        smoke: [{ type: Number }],
        detail: [{ type: String }],
        updated_at: { type: String, default: moment().format() },

    }]




})

const Environment = mongoose.model('Environment', EnvironmentSchema);

module.exports = Environment;