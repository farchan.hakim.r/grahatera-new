const include = require('../routes/includeRouter/include');

const mongoose = include.mongoose;
const moment = include.moment;

const pumpSensorSchema = new mongoose.Schema({
    building_name: { type: String },
    pump_type: { type: String },
    status: [{ type: Boolean, default: false }],
    status_repair: { type: Number, default: 0 },
    updated_at: { type: String, default: moment().format() },
    detail: [{
        _id: false,
        current: [{ type: String }],
        volt: [{ type: String }],
        debit: [{ type: String }],
        level: [{ type: String }],
        status_pump: [{ type: Boolean }],
        alert: { type: String },
        created_at: { type: String, default: moment().format() },
    }],


})

const PumpSensor = mongoose.model('PumpSensor', pumpSensorSchema);

module.exports = PumpSensor;