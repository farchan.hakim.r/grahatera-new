const include = require('./../routes/includeRouter/include');

const moment = include.moment;
const stringify = include.stringify;

const Environment = require('./../models/environmentModel');





async function getEnvironment(req, res, next) {
    if (res.statusCode == 200) {
        Environment.find({ building_name: req.params.building_name, environment_type: req.params.environment_type }, await function(err, EnvironmentFinds) {
            if (EnvironmentFinds.length < 1) {
                res.status(404).json({
                    pesan: "data tidak ditemukan"
                })

            } else {
                let data = {
                    detail_data: EnvironmentFinds[0].detail_data[0],
                    detail_parameter: EnvironmentFinds[0].detail_parameter[0],
                    detail_alert: EnvironmentFinds[0].detail_alert[0],
                    relay_by_hardware: EnvironmentFinds[0].relay_by_hardware,
                    relay_by_software: EnvironmentFinds[0].relay_by_software,
                }
                res.status(200).json({
                    pesan: "data sensor",
                    data: data
                })
            }
        });

    } else {
        res.status(404).json({
            pesan: "token tidak berlaku",
        });
    }
}


async function getAllEnvironment(req, res, next) {
    let start = req.params.start_date;
    let end = req.params.end_date;
    let startDay = moment().startOf('day').format();
    let endDay = moment().endOf('day').format();
    if (start != 1 && end != 1) {

        startDay = moment(start).startOf('day').format();
        endDay = moment(end).endOf('day').format();
        console.log('false')
    }

    if (res.statusCode == 200) {
        Environment.find({ 
            'detail_data.0.updated_at': {
                $gte: startDay,
                $lte: endDay
            }, building_name: req.params.building_name, environment_type: req.params.environment_type }, await function(err, EnvironmentFinds) {
            if (EnvironmentFinds.length < 1) {
                res.status(404).json({
                    pesan: "data tidak ditemukan"
                })

            } else {
                let data = {
                    detail_data: EnvironmentFinds[0].detail_data.map(datas => {
                        return datas
                    }),
                    detail_parameter: EnvironmentFinds[0].detail_parameter.map(datas => {
                        return datas
                    }),
                    detail_alert: EnvironmentFinds[0].detail_alert.map(datas => {
                        return datas
                    }),
                    relay_by_hardware: EnvironmentFinds[0].relay_by_hardware,
                    relay_by_software: EnvironmentFinds[0].relay_by_software,
                }
                res.status(200).json({
                    pesan: "data sensor",
                    data: data
                })
            }
        });

    } else {
        res.status(404).json({
            pesan: "token tidak berlaku",
        });
    }
}
async function saveEnvironment(req, res, next) {
    let data = req.body;
    data.building_name = req.params.building_name;
    data.environment_type = req.params.environment_type;
    data.detail_data[0].updated_at = moment().format();
    data.updated_at = moment().format();
    let environment = new Environment(data);
    let errData = environment.validateSync();

    if (res.statusCode == 200) {
        if (!errData) {
            Environment.find({ building_name: req.params.building_name, environment_type: req.params.environment_type }, await function(err, EnvironmentFinds) {
                if (EnvironmentFinds.length < 1) {
                    environment.save(function(err, result) {
                        if (err) {
                            res.status(400).json({
                                pesan: "Gagal mengisi",
                            });
                        } else {
                            res.status(200).json({
                                pesan: "Berhasil mengisi data",
                                data: data,
                            });
                        }
                    });
                } else {
                    Environment.findOneAndUpdate({ building_name: req.params.building_name, environment_type: req.params.environment_type }, {
                            $push: {
                                'detail_data': {
                                    $each: data.detail_data,
                                    $position: 0,
                                   
                                }

                            }
                        },
                        function(err, sukses) {
                            res.status(200).json({
                                pesan: "tambah data ",
                                data: data
                            });
                        });
                }
            });
        } else {
            res.status(404).json({
                pesan: "data error",
            });
        }
    } else {
        res.status(404).json({
            pesan: "token tidak berlaku",
        });
    }

}

async function saveParameter(req, res, next) {
    let data = req.body;
    data.building_name = req.params.building_name;
    data.environment_type = req.params.environment_type;
    let environment = new Environment(data);
    let errData = environment.validateSync();
    data.detail_parameter[0].updated_at = moment().format();
    if (res.statusCode == 200) {
        if (!errData) {
            Environment.find({ building_name: req.params.building_name, environment_type: req.params.environment_type },await function(err, EnvironmentFinds) {
                if (EnvironmentFinds.length < 1) {
                    environment.save(function(err, result) {
                        if (err) {
                            res.status(400).json({
                                pesan: "Gagal mengisi",
                            });
                        } else {
                            res.status(200).json({
                                pesan: "Berhasil mengisi data",
                                data: data,
                            });
                        }
                    });
                } else {
                    Environment.findOneAndUpdate({ building_name: req.params.building_name, environment_type: req.params.environment_type }, {
                            $push: {
                                'detail_parameter': {
                                    $each: data.detail_parameter,
                                    $position: 0,
                                    $slice: 100
                                }

                            }
                        },
                        function(err, sukses) {
                            res.status(200).json({
                                pesan: "tambah data ",
                                data: data
                            });
                        });
                }
            });
        } else {
            res.status(404).json({
                pesan: "data error",
            });
        }
    } else {
        res.status(404).json({
            pesan: "token tidak berlaku",
        });
    }

}

async function saveControlStatus(req, res, next) {
    let data = req.body;
    data.building_name = req.params.building_name;
    data.environment_type = req.params.environment_type;
    let environment = new Environment(data);
    let errData = environment.validateSync();
    
    if (res.statusCode == 200) {
        if (!errData) {
            Environment.find({ building_name: req.params.building_name, environment_type: req.params.environment_type },await function(err, EnvironmentFinds) {
                if (EnvironmentFinds.length < 1) {
                    environment.save(function(err, result) {
                        if (err) {
                            res.status(400).json({
                                pesan: "Gagal mengisi",
                            });
                        } else {
                            res.status(200).json({
                                pesan: "Berhasil mengisi data",
                                data: data,
                            });
                        }
                    });
                } else {
                    Environment.findOneAndUpdate({ building_name: req.params.building_name, environment_type: req.params.environment_type }, {
                            $set: {
                                'relay_by_software': data.relay_by_software,


                            }
                        },
                        function(err, sukses) {
                            res.status(200).json({
                                pesan: "tambah data ",
                                data: data
                            });
                        });
                }
            });
        } else {
            res.status(404).json({
                pesan: "data error",
                errData
            });
        }
    } else {
        res.status(404).json({
            pesan: "token tidak berlaku",
        });
    }

}

async function saveControlStatusHardware(req, res, next) {
    let data = req.body;
    data.building_name = req.params.building_name;
    data.environment_type = req.params.environment_type;
    let environment = new Environment(data);
    let errData = environment.validateSync();

    if (res.statusCode == 200) {
        if (!errData) {
            Environment.find({ building_name: req.params.building_name, environment_type: req.params.environment_type },await function(err, EnvironmentFinds) {
                if (EnvironmentFinds.length < 1) {
                    environment.save(function(err, result) {
                        if (err) {
                            res.status(400).json({
                                pesan: "Gagal mengisi",
                            });
                        } else {
                            res.status(200).json({
                                pesan: "Berhasil mengisi data",
                                data: data,
                            });
                        }
                    });
                } else {

                    Environment.findOneAndUpdate({ building_name: req.params.building_name, environment_type: req.params.environment_type }, {
                            $set: {
                                'relay_by_hardware': data.relay_by_hardware,
                            }
                        },
                        function(err, sukses) {
                            res.status(200).json({
                                pesan: "tambah data ",
                                data: data
                            });
                        });
                }
            });
        } else {
            res.status(404).json({
                pesan: "data error",
                errData
            });
        }
    } else {
        res.status(404).json({
            pesan: "token tidak berlaku",
        });
    }

}


async function saveAlert(req, res, next) {
    let data = req.body;
    data.building_name = req.params.building_name;
    data.environment_type = req.params.environment_type;
    let environment = new Environment(data);
    let errData = environment.validateSync();
    data.detail_alert[0].updated_at = moment().format();
    if (res.statusCode == 200) {
        if (!errData) {
            let detail = [];
            for (var i = 0; i < data.detail_alert[0].temperature.length; i++) {
                let val = data.detail_alert[0].temperature[i]
                if (val == 0) detail.push("Temepratur dibawah normal ");
                else if (val == 2) detail.push("Temepratur diatas normal " );
            }
            for (var i = 0; i < data.detail_alert[0].humidity.length; i++) {
                let val = data.detail_alert[0].humidity[i]
                if (val == 0) detail.push("Humidity dibawah normal  " );
                else if (val == 2) detail.push("Humidity diatas normal" );
            }
            for (var i = 0; i < data.detail_alert[0].voltage.length; i++) {
                let val = data.detail_alert[0].voltage[i]
                if (val == 0) detail.push("Voltage dibawah normal  " );
                else if (val == 2) detail.push("Voltage diatas normal pada data ke " + i);
            }
            for (var i = 0; i < data.detail_alert[0].smoke.length; i++) {
                let val = data.detail_alert[0].smoke[i]
                if (val == 0) detail.push("Smoke dibawah normal pada data ke " + i);
                else if (val == 2) detail.push("Smoke diatas normal pada data ke " + i);
            }
            data.detail_alert[0].detail = detail;
            Environment.find({ building_name: req.params.building_name, environment_type: req.params.environment_type },await function(err, EnvironmentFinds) {
                if (EnvironmentFinds.length < 1) {
                    let environmentFIx = new Environment(data);
                    environmentFIx.save(function(err, result) {
                        if (err) {
                            res.status(400).json({
                                pesan: "Gagal mengisi",
                            });
                        } else {
                            res.status(200).json({
                                pesan: "Berhasil mengisi data",
                                data: data,
                            });
                        }
                    });
                } else {
                    Environment.findOneAndUpdate({ building_name: req.params.building_name, environment_type: req.params.environment_type }, {
                            $push: {
                                'detail_alert': {
                                    $each: data.detail_alert,
                                    $position: 0,
                                    $slice: 100
                                }

                            }
                        },
                        function(err, sukses) {
                            res.status(200).json({
                                pesan: "tambah data ",
                                data: data
                            });
                        });
                }
            });
        } else {
            res.status(404).json({
                pesan: "data error",
            });
        }
    } else {
        res.status(404).json({
            pesan: "token tidak berlaku",
        });
    }

}




async function saveStatusRepair(req, res, next) {
    let data = req.body;
    data.building_name = req.params.building_name;
    data.environment_type = req.params.environment_type;
    let environment = new Environment(data);
    let errData = environment.validateSync();

    if (res.statusCode == 200) {
        if (!errData) {
            Environment.find({ building_name: req.params.building_name, environment_type: req.params.environment_type },await function(err, EnvironmentFinds) {
                if (EnvironmentFinds.length < 1) {
                    environment.save(function(err, result) {
                        if (err) {
                            res.status(400).json({
                                pesan: "Gagal mengisi",
                            });
                        } else {
                            res.status(200).json({
                                pesan: "Berhasil mengisi data",
                                data: data,
                            });
                        }
                    });
                } else {
                    Environment.findOneAndUpdate({ building_name: req.params.building_name, environment_type: req.params.environment_type }, {
                            $set: {
                                'status_repair': data.status_repair,


                            }
                        },
                        function(err, sukses) {
                            res.status(200).json({
                                pesan: "tambah data ",
                                data: data
                            });
                        });
                }
            });
        } else {
            res.status(404).json({
                pesan: "data error",
                errData
            });
        }
    } else {
        res.status(404).json({
            pesan: "token tidak berlaku",
        });
    }

}

async function deleteEnvironment(req, res, next) {
    let start = req.params.start_date;
    let end = req.params.end_date;
    let startDay = moment().startOf('day').format();
    let endDay = moment().endOf('day').format();
    if (start != 1 && end != 1) {

        startDay = moment(start).startOf('day').format();
        endDay = moment(end).endOf('day').format();
   
    }

    if (res.statusCode == 200) {
        Environment.deleteMany({ 
            'detail_data.updated_at': {
                $gte: startDay,
                $lte: endDay
            }, building_name: req.params.building_name, environment_type: req.params.environment_type },await function(err, EnvironmentFinds) {
            if (EnvironmentFinds.length < 1) {
                res.status(404).json({
                    pesan: "data tidak ditemukan"
                })

            } else {
           
                res.status(200).json({
                    pesan: "data sensor berhasil di remove",
                   
                })
            }
        });

    } else {
        res.status(404).json({
            pesan: "token tidak berlaku",
        });
    }
}
exports.saveEnvironment = saveEnvironment;
exports.saveParameter = saveParameter;
exports.saveControlStatus = saveControlStatus;
exports.saveControlStatusHardware = saveControlStatusHardware;
exports.saveAlert = saveAlert;
exports.getEnvironment = getEnvironment;
exports.saveStatusRepair = saveStatusRepair;
exports.getAllEnvironment = getAllEnvironment;
exports.deleteEnvironment = deleteEnvironment;