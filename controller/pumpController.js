const include = require('./../routes/includeRouter/include');

const moment = include.moment;
const stringify = include.stringify;

const Pump = require('./../models/pumpModel');

const startDay = moment().startOf('day').format();
const endDay = moment().endOf('day').format();



function getPump(req, res, next) {
    if (res.statusCode == 200) {
        Pump.find({ building_name: req.params.building_name, pump_type: req.params.pump_type }, function(err, pumpFinds) {
            if (pumpFinds.length < 1) {
                res.status(404).json({
                    pesan: "data tidak ditemukan"
                })

            } else {
                res.status(200).json({
                    pesan: "data sensor",
                    data: pumpFinds[0]
                })
            }
        });

    } else {
        res.status(404).json({
            pesan: "token tidak berlaku",
        });
    }
}
// FOR PATIENT CHECKUP REGISTER/UPDATE
function savePump(req, res, next) {
    let data = req.body;
    data.building_name = req.params.building_name;
    data.pump_type = req.params.pump_type;
    let pump = new Pump(data);
    let errData = pump.validateSync();

    if (res.statusCode == 200) {
        if (!errData) {
            Pump.find({ building_name: req.params.building_name, pump_type: req.params.pump_type }, function(err, pumpFinds) {
                if (pumpFinds.length < 1) {
                    pump.save(function(err, result) {
                        if (err) {
                            res.status(400).json({
                                pesan: "Gagal mengisi",
                            });
                        } else {
                            res.status(200).json({
                                pesan: "Berhasil mengisi data",
                                data: data,
                            });
                        }
                    });
                } else {
                    Pump.findOneAndUpdate({ building_name: req.params.building_name, pump_type: req.params.pump_type }, {
                            $push: {
                                'detail': {
                                    $each: data.detail,
                                    $position: 0,

                                }
                            }
                        },
                        function(err, patient) {
                            res.status(200).json({
                                pesan: "tambah data ",
                                data: data
                            });
                        });
                }
            });
        } else {
            res.status(404).json({
                pesan: "data error",
            });
        }
    } else {
        res.status(404).json({
            pesan: "token tidak berlaku",
        });
    }

}

exports.savePump = savePump;
exports.getPump = getPump;