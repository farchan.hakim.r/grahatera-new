var express = require('express');
var router = express.Router();

const EnvironmentController = require('./../controller/kitchenController');
const auth = require('./../middleware/authentication')

//BUAT POST DATA
router.post('/data/:building_name/:environment_type', EnvironmentController.saveEnvironment, function(req, res, next) {});
router.post('/param/:building_name/:environment_type', EnvironmentController.saveParameter, function(req, res, next) {});

//UPDATE STATUS
router.post('/control_software/:building_name/:environment_type', EnvironmentController.saveControlStatus, function(req, res, next) {});
router.post('/control_hardware/:building_name/:environment_type', EnvironmentController.saveControlStatusHardware, function(req, res, next) {});

//SET STATUS REPAIR
router.post('/repair/:building_name/:environment_type', EnvironmentController.saveStatusRepair, function(req, res, next) {});

//SET ALERT
router.post('/alert/:building_name/:environment_type', EnvironmentController.saveAlert, function(req, res, next) {});

//GET ALL DATA
router.get('/:building_name/:environment_type', EnvironmentController.getEnvironment, function(req, res, next) {});
router.put('/:building_name/:environment_type/:start_date/:end_date', EnvironmentController.getAllEnvironment, function(req, res, next) {});

router.purge('/:building_name/:environment_type/:start_date/:end_date', EnvironmentController.deleteEnvironment, function(req, res, next) {});

module.exports = router;