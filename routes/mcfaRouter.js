var express = require('express');
var router = express.Router();

const McfaController = require('./../controller/mcfaController');
const auth = require('./../middleware/authentication');



//buat kirim mcfa zona
router.post('/zona/:building_name/:mcfa_type', auth.verifyToken, McfaController.setZona, function(req,res,next) {} );

//buat kirim mcfa data

router.post('/:building_name/:mcfa_type', auth.verifyToken, McfaController.setMcfa, function(req,res,next) {} );

//buat ambil mcfa data
router.get('/:building_name/:mcfa_type', auth.verifyToken, McfaController.getMcfa, function(req,res,next) {} );

router.get('/:building_name/:mcfa_type/ALL', auth.verifyToken, McfaController.getAllMcfa, function(req,res,next) {} );


//eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlblN0YWZmIjp7Im5hbWUiOiJncmFoYXRlcmEiLCJwYXNzd29yZCI6IiQyYSQxMiRaYVNqL296MWNGOWZnYlU3MDlOREdlYlBoNGRRdGNBL01peFJXMXBMdFVyY0poSEljQXNTUyJ9LCJpYXQiOjE1NTM2NjU1Mjd9.H4ZBYpQJnpChK4RzuIOBbcJgnVJQbuN5iEfH71DLJgM

module.exports = router;
